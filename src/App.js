import React, { Component } from 'react';
import ButtonAppBar from './ButtonAppBar';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import axios from 'axios';

import './App.css';

// create a function to call the API wheather
async function getWeatherData(){
  const resp = await axios.get("https://api.openweathermap.org/data/2.5/weather?id=1642907&appid=18cede6b73a127ed47992ee400a1d2bd")
  return resp;

}

class App extends Component {

  constructor(props){
    super(props);
    this.handleClick = this.handleClick.bind(this)

    this.state = {
      weather: []
    };
  }

  handleClick(e){
    console.log("get weather data.......");
    getWeatherData().then(
      result =>{
        this.setState({
          weather: result.data
        })
        console.log(JSON.stringify(this.state.weather))
        
      }
    )
  }

  render(){
    return (
      <div padding="5px">
        <ButtonAppBar />  
        <Grid container spacing={8}>
          <Grid item xs={12}>
          <Button variant="contained" color="secondary" onClick={this.handleClick}>
            GET WEATHER
          </Button>
          </Grid>
          <Grid item xs={12} padding="5px">
            <TextField
              id="Weather Result"
              label="Weather Result"
              multiline
              rowsMax="25"
              padding="5px"
              margin="normal"
              helperText="API URL https://api.openweathermap.org/data/2.5/weather?id=1642907&appid=18cede6b73a127ed47992ee400a1d2bd"
              variant="outlined"
              fullWidth="true"
              value={JSON.stringify(this.state.weather)}
    
            />
          </Grid>
        
        </Grid>
        
      </div>
      
      );
  }

  
}

export default App;
